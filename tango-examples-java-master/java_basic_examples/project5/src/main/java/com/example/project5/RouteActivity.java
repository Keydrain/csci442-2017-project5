package com.example.project5;

import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.Tango.OnTangoUpdateListener;
import com.google.atap.tangoservice.TangoConfig;
import com.google.atap.tangoservice.TangoCoordinateFramePair;
import com.google.atap.tangoservice.TangoErrorException;
import com.google.atap.tangoservice.TangoEvent;
import com.google.atap.tangoservice.TangoInvalidException;
import com.google.atap.tangoservice.TangoOutOfDateException;
import com.google.atap.tangoservice.TangoPointCloudData;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static java.lang.Math.abs;
import static java.lang.Math.round;

public class RouteActivity extends AppCompatActivity {

    private static final String TAG = AreaActivity.class.getSimpleName();
    private static final int SECS_TO_MILLISECS = 1000;
    private Tango mTango;
    private TangoConfig mConfig;
    private TextView mRelocalizationTextView;
    private TextView mXPositionTextView;
    private TextView mYPositionTextView;
    private TextView mZPositionTextView;
    private TextView mXDifferenceTextView;
    private TextView mYDifferenceTextView;
    private TextView mZDifferenceTextView;
    private TextView mCongrats;

    private EditText mXTargetEditView;
    private EditText mYTargetEditView;
    private EditText mZTargetEditView;

    private float[] target = new float[3];
    private boolean hasTarget = false;

    private double mPreviousPoseTimeStamp;
    private double mTimeToNextUpdate = UPDATE_INTERVAL_MS;

    private boolean mIsRelocalized;

    private static final double UPDATE_INTERVAL_MS = 100.0;

    private final Object mSharedLock = new Object();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routing);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Initialize Tango Service as a normal Android Service. Since we call mTango.disconnect()
        // in onPause, this will unbind Tango Service, so every time onResume gets called we
        // should create a new Tango object.
        mTango = new Tango(RouteActivity.this, new Runnable() {
            // Pass in a Runnable to be called from UI thread when Tango is ready; this Runnable
            // will be running on a new thread.
            // When Tango is ready, we can call Tango functions safely here only when there are no
            // UI thread changes involved.
            @Override
            public void run() {
                synchronized (RouteActivity.this) {
                    try {
                        mConfig = setTangoConfig(mTango);
                        mTango.connect(mConfig);
                        startupTango();
                    } catch (TangoOutOfDateException e) {
                        Log.e(TAG, getString(R.string.tango_out_of_date_exception), e);
                        showsToastAndFinishOnUiThread(R.string.tango_out_of_date_exception);
                    } catch (TangoErrorException e) {
                        Log.e(TAG, getString(R.string.tango_error), e);
                        showsToastAndFinishOnUiThread(R.string.tango_error);
                    } catch (TangoInvalidException e) {
                        Log.e(TAG, getString(R.string.tango_invalid), e);
                        showsToastAndFinishOnUiThread(R.string.tango_invalid);
                    } catch (SecurityException e) {
                        // Area Learning permissions are required. If they are not available,
                        // SecurityException is thrown.
                        Log.e(TAG, getString(R.string.no_permissions), e);
                        showsToastAndFinishOnUiThread(R.string.no_permissions);
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (RouteActivity.this) {
                            setupTextViewsAndButtons();
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Clear the relocalization state; we don't know where the device will be since our app
        // will be paused.
        mIsRelocalized = false;
        synchronized (this) {
            try {
                mTango.disconnect();
            } catch (TangoErrorException e) {
                Log.e(TAG, getString(R.string.tango_error), e);
            }
        }
    }

    private void setupTextViewsAndButtons() {

        mCongrats = (TextView) findViewById(R.id.Congrats);
        updateTextView(mCongrats, getString(R.string.empty));

        TextView mUuidTextView = (TextView) findViewById(R.id.adf_uuid_textview);
        mRelocalizationTextView = (TextView) findViewById(R.id.relocalization_textview);

        mXPositionTextView = (TextView) findViewById(R.id.xPos);
        mYPositionTextView = (TextView) findViewById(R.id.yPos);
        mZPositionTextView = (TextView) findViewById(R.id.zPos);

        mXDifferenceTextView = (TextView) findViewById(R.id.xDir);
        mYDifferenceTextView = (TextView) findViewById(R.id.yDir);
        mZDifferenceTextView = (TextView) findViewById(R.id.zDir);

        mXTargetEditView = (EditText) findViewById(R.id.xInput);
        mYTargetEditView = (EditText) findViewById(R.id.yInput);
        mZTargetEditView = (EditText) findViewById(R.id.zInput);

        ArrayList<String> fullUuidList;
        fullUuidList = mTango.listAreaDescriptions();
        if (fullUuidList.size() == 0) {
            mUuidTextView.setText(R.string.no_uuid);
        } else {
            mUuidTextView.setText(getString(R.string.latest_adf_is) + fullUuidList.get(fullUuidList.size() - 1));
        }
    }

    private TangoConfig setTangoConfig(Tango tango) {
        // Use default configuration for Tango Service.
        TangoConfig config = tango.getConfig(TangoConfig.CONFIG_TYPE_DEFAULT);
        // Check if learning mode.
        // Set learning mode to config.
        ArrayList<String> fullUuidList;
        // Returns a list of ADFs with their UUIDs.
        fullUuidList = tango.listAreaDescriptions();
        // Load the latest ADF if ADFs are found.
        if (fullUuidList.size() > 0) {
            config.putString(TangoConfig.KEY_STRING_AREADESCRIPTION,
                    fullUuidList.get(fullUuidList.size() - 1));
        }
        return config;
    }

    private void startupTango() {
        // Set Tango listeners for Poses Device wrt Start of Service, Device wrt
        // ADF and Start of Service wrt ADF.
        ArrayList<TangoCoordinateFramePair> framePairs = new ArrayList<TangoCoordinateFramePair>();
        framePairs.add(new TangoCoordinateFramePair(
                TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                TangoPoseData.COORDINATE_FRAME_DEVICE));
        framePairs.add(new TangoCoordinateFramePair(
                TangoPoseData.COORDINATE_FRAME_AREA_DESCRIPTION,
                TangoPoseData.COORDINATE_FRAME_DEVICE));
        framePairs.add(new TangoCoordinateFramePair(
                TangoPoseData.COORDINATE_FRAME_AREA_DESCRIPTION,
                TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE));

        mTango.connectListener(framePairs, new OnTangoUpdateListener() {

            @Override
            public void onPoseAvailable(TangoPoseData pose) {
                mRelocalizationTextView = (TextView) findViewById(R.id.relocalization_textview);
                // Make sure to have atomic access to Tango data so that UI loop doesn't interfere
                // while Pose call back is updating the data.
                synchronized (mSharedLock) {
                    // Check for Device wrt ADF pose, Device wrt Start of Service pose, Start of
                    // Service wrt ADF pose (this pose determines if the device is relocalized or
                    // not).
                    if (pose.baseFrame == TangoPoseData.COORDINATE_FRAME_AREA_DESCRIPTION
                            && pose.targetFrame == TangoPoseData
                            .COORDINATE_FRAME_START_OF_SERVICE) {
                        mIsRelocalized = pose.statusCode == TangoPoseData.POSE_VALID;
                    }
                }

                final double deltaTime = (pose.timestamp - mPreviousPoseTimeStamp) *
                        SECS_TO_MILLISECS;
                mPreviousPoseTimeStamp = pose.timestamp;
                mTimeToNextUpdate -= deltaTime;

                if (mTimeToNextUpdate < 0.0) {
                    mTimeToNextUpdate = UPDATE_INTERVAL_MS;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            synchronized (mSharedLock) {
                                mRelocalizationTextView.setText(mIsRelocalized ?
                                        getString(R.string.localized) :
                                        getString(R.string.not_localized));
                            }
                        }
                    });
                }
                updatePose(pose);
            }

            @Override
            public void onXyzIjAvailable(TangoXyzIjData xyzIj) {
                // We are not using onXyzIjAvailable for this app.
            }

            @Override
            public void onPointCloudAvailable(TangoPointCloudData xyzij) {
                // We are not using onPointCloudAvailable for this app.
            }

            @Override
            public void onTangoEvent(final TangoEvent event) {
                // Ignoring TangoEvents.
            }

            @Override
            public void onFrameAvailable(int cameraId) {
                // We are not using onFrameAvailable for this application.
            }
        });
    }

    private void updatePose(TangoPoseData pose) {

        final float translation[] = pose.getTranslationAsFloats();

        float xPosition = round(translation[0]);
        float yPosition = round(translation[1]);
        float zPosition = round(translation[2]);

        updateTextView(mXPositionTextView, getString(R.string.xPos) + xPosition + getString(R.string.units));
        updateTextView(mYPositionTextView, getString(R.string.yPos) + yPosition + getString(R.string.units));
        updateTextView(mZPositionTextView, getString(R.string.zPos) + zPosition + getString(R.string.units));

        if (hasTarget) {

            float xDiff = (int) target[0] - xPosition;
            float yDiff = (int) target[1] - yPosition;
            float zDiff = (int) target[2] - zPosition;

            if (xDiff < 0) {
                updateTextView(mXDifferenceTextView, getString(R.string.xDirLeft) + abs(xDiff) + getString(R.string.units));
            } else if (xDiff == 0) {
                updateTextView(mXDifferenceTextView, getString(R.string.xDirStay));
            } else {
                updateTextView(mXDifferenceTextView, getString(R.string.xDirRight) + xDiff + getString(R.string.units));
            }
            if (yDiff < 0) {
                updateTextView(mYDifferenceTextView, getString(R.string.yDirBackward) + abs(yDiff) + getString(R.string.units));
            }  else if (yDiff == 0) {
                updateTextView(mYDifferenceTextView, getString(R.string.yDirStay));
            } else {
                updateTextView(mYDifferenceTextView, getString(R.string.yDirForward) + yDiff + getString(R.string.units));
            }
            if (zDiff < 0) {
                updateTextView(mZDifferenceTextView, getString(R.string.zDirDown) + abs(zDiff) + getString(R.string.units));
            }  else if (zDiff == 0) {
                updateTextView(mZDifferenceTextView, getString(R.string.zDirStay));
            } else {
                updateTextView(mZDifferenceTextView, getString(R.string.zDirUp) + zDiff + getString(R.string.units));
            }

            if (xDiff == 0 && yDiff == 0 && zDiff == 0) {
                updateTextView(mCongrats, getString(R.string.congrats));
            } else {
                updateTextView(mCongrats, getString(R.string.empty));
            }

        }

    }

    public void goAdfClicked(View view) {
        float tarX = Float.parseFloat(mXTargetEditView.getText().toString().substring(getString(R.string.xTar).length()));
        float tarY = Float.parseFloat(mYTargetEditView.getText().toString().substring(getString(R.string.yTar).length()));
        float tarZ = Float.parseFloat(mZTargetEditView.getText().toString().substring(getString(R.string.zTar).length()));
        target[0] = tarX;
        target[1] = tarY;
        target[2] = tarZ;
        hasTarget = true;
    }

    private void updateTextView(final TextView view, final String content) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                synchronized (mSharedLock) {
                    view.setText(content);
                }
            }
        });
    }


    private void showsToastAndFinishOnUiThread(final int resId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(RouteActivity.this,
                        getString(resId), Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }
}
