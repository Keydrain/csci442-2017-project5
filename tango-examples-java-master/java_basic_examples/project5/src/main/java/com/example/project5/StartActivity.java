package com.example.project5;

import com.google.atap.tangoservice.Tango;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.os.Bundle;
import android.widget.Toast;

public class StartActivity extends AppCompatActivity {

    // Permission request action.
    public static final int REQUEST_CODE_TANGO_PERMISSION = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        setTitle(R.string.app_name);

        startActivityForResult(
                Tango.getRequestPermissionIntent(Tango.PERMISSIONTYPE_ADF_LOAD_SAVE), 0);
    }

    public void loadADFClicked(View v) {
        startLoadADFActivity();
    }

    public void recADFClicked(View v) {
        startRecordADFActivity();
    }

    private void startLoadADFActivity() {
        Intent loadADFIntent = new Intent(this, RouteActivity.class);
        startActivity(loadADFIntent);
    }

    private void startRecordADFActivity() {
        Intent recADFIntent = new Intent(this, AreaActivity.class);
        startActivity(recADFIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // The result of the permission activity.
        //
        // Note that when the permission activity is dismissed, the HelloAreaDescriptionActivity's
        // onResume() callback is called. Because the Tango Service is connected in the onResume()
        // function, we do not call connect here.
        //
        // Check which request we're responding to.
        if (requestCode == REQUEST_CODE_TANGO_PERMISSION) {
            // Make sure the request was successful.
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, R.string.arealearning_permission, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}
